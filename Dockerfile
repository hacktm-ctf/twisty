FROM ubuntu:16.04
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y lib32z1 xinetd
RUN useradd -d /home/pwn -u 8888 -m pwn
COPY ./xinetd /etc/xinetd.d/xinetd
COPY ./share /home/pwn/
RUN chmod 1755 /home/pwn; chown root:root /home/pwn /home/pwn/twisty /home/pwn/flag /home/pwn/run.sh; chmod 755 /home/pwn/twisty /home/pwn/run.sh; chmod 644 /home/pwn/flag
CMD ["/usr/sbin/xinetd", "-dontfork"]

