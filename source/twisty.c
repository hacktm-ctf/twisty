#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void add_move(char inp, char *move_buf, int *move_count) {
	if(*move_count % 2 == 0) {
		move_buf[*move_count/2] = inp << 4;
	} else {
		move_buf[*move_count/2] += inp;
	}

	*move_count = *move_count + 1;
}

char undo_last_move(char *move_buf, int *move_count) {
	*move_count = *move_count - 1;

	char last;

	if(*move_count % 2 == 0) {
		last = move_buf[*move_count/2] >> 4;
		move_buf[*move_count/2] = 0;
	} else {
		last = move_buf[*move_count/2] & 15;
		move_buf[*move_count/2] &= 0xf0;
	}

	return last;
}

void move_cube(char inp, char *cube) {
	int v = inp & 3;
	if((inp & 8) == 0) {
		// vertical
		if((inp & 4) == 0) {
			// up
			char aux = cube[v];
			cube[v] = cube[v+4];
			cube[v+4] = cube[v+8];
			cube[v+8] = cube[v+12];
			cube[v+12] = aux;
		} else {
			// down
			char aux = cube[v+12];
			cube[v+12] = cube[v+8];
			cube[v+8] = cube[v+4];
			cube[v+4] = cube[v];
			cube[v] = aux;
		}
	} else {
		// horizontal
		cube += 4*v;
		if((inp & 4) == 0) {
			// right
			char aux = cube[3];
			cube[3] = cube[2];
			cube[2] = cube[1];
			cube[1] = cube[0];
			cube[0] = aux;
		} else {
			// left
			char aux = cube[0];
			cube[0] = cube[1];
			cube[1] = cube[2];
			cube[2] = cube[3];
			cube[3] = aux;
		}
	}
}

void print_cube(char *cube) {
	for(int i=0; i<4; i++) {
		for(int j=0; j<4; j++) {
			printf("%c", cube[i*4+j]);
		}
		printf("\n");
	}
}

void shuffle_cube(char *cube) {
	FILE *du = fopen("/dev/urandom", "rb");
	for(int i=0; i<500; i++) {
		move_cube(getc(du) & 0xf, cube);
	}
	fclose(du);
}

int main() {
	struct {
		unsigned char uhh[8];
		unsigned char cube[16];
		unsigned char move_buf[2048];
		int move_halves;
	} l;

	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);

	//alarm(64);

	l.move_halves = 0;
	memcpy(l.cube, "ABCDEFGHIJKLMNOP", 16);

	puts(
"Welcome to my game!\n"
"Your job is to get the \"rubik's square\" to this configuration:\n");
	print_cube(l.cube);
	puts("\nGood luck!\n\n");

	shuffle_cube(l.cube);

	while(memcmp(l.cube, "ABCDEFGHIJKLMNOP", 16)) {
		print_cube(l.cube);
		char inp;
		printf("> ");
		do {
			inp = getchar();
		} while(inp == '\n');
		if(inp == 'c' || inp == 'r') {
			char v = getchar() - '0', d = getchar();
			if(v < 0 || v > 3) {
				printf("Bad row/column number %d\n", v);
				exit(1);
			}
			char mov = -1;
			if(inp == 'c') {
				if(d == 'd') {
					mov = 4+v;
				} else if(d == 'u') {
					mov = v;
				} else {
					printf("Bad column direction '%c'\n", d);
					exit(1);
				}
			} else {
				if(d == 'l') {
					mov = 12+v;
				} else if(d == 'r') {
					mov = 8+v;
				} else {
					printf("Bad row direction '%c'\n", d);
					exit(1);
				}
			}
			if(mov<0 || mov>15) {
				printf("Unexpected error\n");
				exit(1);
			}
			add_move(mov, l.move_buf, &l.move_halves);
			move_cube(mov, l.cube);
		} else if(inp == '?') {
			puts(
"Commands:\n"
"    ? - print this message\n"
"  cXd - rotate column X down\n"
"  cXu - rotate column X up\n"
"  rXl - rotate row X left\n"
"  rXr - rotate row X right\n"
"    l - list previous moves\n"
"    u - undo last inp\n");
		} else if(inp == 'l') {
			for(int i=0; i<l.move_halves; i++) {
				unsigned char m;
				if(i%2==0) {
					m = (l.move_buf[i/2] >> 4) & 0x0f;
				} else {
					m = l.move_buf[i/2] & 0x0f;
				}
				if(m&8) {
					printf("r%c%c ", '0'+(m&3), (m&4) ? 'l' : 'r');
				} else {
					printf("c%c%c ", '0'+(m&3), (m&4) ? 'd' : 'u');
				}
			}
			printf("\n");
		} else if(inp == 'u') {
			char m = undo_last_move(l.move_buf, &l.move_halves);
			move_cube(m ^ 4, l.cube);
		} else {
			puts("Invalid command");
		}
	}

	puts("Congratulations!!!");
}

/*
Attack plan:
overflow move buffer, skip move count to something bigger
leak libc AND stack canary
go back to the return pointer, place rop chain
solve cube
*/
